@tool
extends MeshInstance3D

## How to use RenderingServer.instances_cull_aabb() in an editor tool
##
## There are three methods in the RenderingServer class that are
## intended for use (only) in the Editor.
## Here I explore one that returns an array of all VisualInstance nodes
## that intersect a given AABB.
##
## The way I found to get it to work is to use await. You may be able
## to use func _physics_process(delta: float) to get the same result.
## This depends on your plugin/tool and what it does.

## Run the probe and see a printout of hits.
@export var do_probe:bool:
	set(b):
		do_probe = false
		print()
		print("PROBING")
		var hits:Array = await overlapping(self.transform)
		for objid in hits:
			var i = instance_from_id(objid)
			print("Probe hit:", i.name)


# This makes use of Godot's built-in BVH volume system. It's a bit
# clunky, but it's better than anything I can make.
# Note: BVH only works on VisualInstance derived NODES. (Not geometry made
# with server methods)
func overlapping(t:Transform3D) -> Array:
	var local_aabb:AABB = self.mesh.get_aabb()

	# get_aabb is in a mesh-local-space
	# it must be moved to t (inside our space)
	var global_aabb:AABB = t * local_aabb

	# To avoid detecting ourselves, we go invisible
	self.visible = false

	# NB await BEFORE the cull_aabb.
	# IT WON'T WORK AT ALL ELSE.
	await get_tree().physics_frame

	# The "space" that instances_cull_aabb works in is a bit vague to me.
	# It seems to work when the space of the probe and the space of the
	# targets is the same. Keep playing with that until stuff happens.
	var pa:PackedInt64Array = RenderingServer.instances_cull_aabb(
			global_aabb, get_world_3d().scenario)

	self.visible = true

	if pa:
		# For some reason, there are many repeats within the
		# returned array. So I removed dups.
		var a:Array = dedup(Array(pa))
		return a
	return []

func dedup(array: Array) -> Array:
	var unique: Array = []
	for item in array:
		if not unique.has(item):
			unique.append(item)
	return unique
